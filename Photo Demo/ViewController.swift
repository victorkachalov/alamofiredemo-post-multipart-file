//
//  ViewController.swift
//  Photo Demo
//
//  Created by Tak Tran on 20/10/2016.
//  Copyright © 2016 Appfish. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.image = image
        } else {
            print ("There was a problem with the image")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func importImage(_ sender: AnyObject) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePickerController.allowsEditing = false
        self.present(imagePickerController, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func postImage(_ sender: Any) { //start: ====== ЗАГРУЗКА ФАЙЛА  ======
        
        
        //взято отсюда https://stackoverflow.com/questions/26121827/uploading-file-with-parameters-using-alamofire
        //помогло еще https://stackoverflow.com/questions/42024868/how-to-send-data-in-alamofire
        
        
        if let data = UIImagePNGRepresentation(imageView.image!) {
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    multipartFormData.append(data, withName: "test")
            },
                to: "https://httpbin.org/post",
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.response { response in
                            print(response)
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                    }
            })
        }
    } // end: ===================== ЗАГРУЗКА ФАЙЛА  ==================================
    
    
    
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

